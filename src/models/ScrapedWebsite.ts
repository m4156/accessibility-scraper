export enum ACCESSIBILITY_CONFORMITY {
  TOTALEMENT_CONFORME= 'TOTALEMENT_CONFORME',
  PARTIELLEMENT_CONFORME= 'PARTIELLEMENT_CONFORME',
  NON_CONFORME= 'NON_CONFORME'
}

export type ScrapedWebsite = {
  url: string;                                            // The homepage url
  homepage_accessibility?: boolean;                       // If the accessibility button is on home page then it's true
  audited?: boolean                                       // if the website had a RGAA audit
  accessibility_page_url?: string;                        // The url of the accessibility page
  accessibility_percent?: number;                         // The accessibility percent (Pourcentage des critères RGAA respectés)
  accessibility_conformity?: ACCESSIBILITY_CONFORMITY;    // accessibility_conformity is 'conforme' or 'non conforme' etc... Deducted from the accessibility_percent
  accessibility_average_rate_percent?: number;            // Not in all website in the average percent per page (Taux moyen du service en ligne)
  err_desc?: string;                                      // if a error is occurs during the scrapping, store the value here, for a manual check
  manual_check: boolean                                   // if the url is manual_checked
}

export const DEFAULT_SCRAPED_WEBSITE = (url: string): ScrapedWebsite => {
  return {
    url: url,
    homepage_accessibility: false,
    audited: false,
    accessibility_page_url: undefined,
    accessibility_percent: undefined,
    accessibility_conformity: undefined,
    accessibility_average_rate_percent: undefined,
    err_desc: undefined,
    manual_check: false
  }
}

// This method is used to get the accessibility conformity from the accessibility percent
export const getAccessibilityConformityFromPercent = (percent: number): ACCESSIBILITY_CONFORMITY => {
  if (percent == 100) {
    return ACCESSIBILITY_CONFORMITY.TOTALEMENT_CONFORME
  } else if (percent >= 50 ) {
    return ACCESSIBILITY_CONFORMITY.PARTIELLEMENT_CONFORME
  } else if (percent >= 0) {
    return ACCESSIBILITY_CONFORMITY.NON_CONFORME
  } else {
    throw new Error(`Invalid percent for the ACCESSIBILITY_CONFORMITY : ${percent}`);
  }
}
