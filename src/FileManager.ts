import path from "path";
import * as appRootPath from "app-root-path";

export const getInputFileDataPath = (fileName: string): string => {
    return path.join(appRootPath.path, 'data', fileName);
};

export const getOutputFileDataPath = (outputIdentifier: string): string => {
    return path.join(appRootPath.path, 'data', generateOutputFileName(outputIdentifier));
};

const generateOutputFileName = (outputIdentifier: string): string => {
    let timestamp = new Date().toISOString().replace(/[-:.]/g,"");
    return `output_${outputIdentifier}_${timestamp}.csv`;
}

export const getExtensionIDontCareAboutCookiesPath = (): string => {
    return path.join(appRootPath.path, 'data','extensions', 'I-don-t-care-about-cookies')
}
