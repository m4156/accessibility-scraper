import * as csvParser from '@fast-csv/parse';
import {getInputFileDataPath, getOutputFileDataPath} from "../FileManager";
import {writeToPath} from "@fast-csv/format";
import puppeteer from "puppeteer";

// Main function
// The goal of this function is to parse all the .fr domain from a csv and keep only the unique and active .gouv.fr domain
//
// There is 3 steps in this script :
// - the first step is to parse all the link to only keep the .gouv.fr domain
// - the second step, for each .gouv.fr domain, is to check if the domain is active (is the server respond with a 200 code)
// - the third step, for each active .gouv.fr domain, we check if the domain redirect to another domain
(async () => {
    const ALL_FR_DOMAIN_FILE_NAME = process.argv[2];     // the input filename
    const COLUMN_NAME_URL = process.argv[3];             // the column name of the url in the input file

    // Log error if missing argument
    if (!ALL_FR_DOMAIN_FILE_NAME || !COLUMN_NAME_URL) {
        console.error("Usage: npm run start:gouv <input-file-name> <column-name-url>. See the README.md for more information.");
        process.exit(1);
    }

    let gouvUrls: string[] = [];                      // This variable is all the .gouv.fr domain
    let activeGouvUrls: string[] = [];                // This variable is all the .gouv.fr domain that are active (server respond with a 200 code)
    let needACheckGouvUrls: string[] = [];            // This variable is all the .gouv.fr domain that need a manual check (server respond with other than a 200 code)
    const uniqueAndActiveGouvUrls = new Set<string>();// This variable is all the unique and active .gouv.fr domain

    const browser = await puppeteer.launch({     // Init puppeteer. We use a browser to avoid security issue
        headless: false,
    })
    const page = await browser.newPage()

    console.log('Parsing csv...');
    csvParser.parseFile(getInputFileDataPath(ALL_FR_DOMAIN_FILE_NAME), {headers: true, delimiter: ';'})
        .on('error', (error) => {console.error(error)})
        .on('data', async (row) => {      // For each row in the csv
            const url = row[COLUMN_NAME_URL]           // Get the url from the csv

            if (/\.gouv\.fr$/gi.test(url)) {           // First step : only keep the .gouv.fr domain
                console.log(`Found : ${url}`);
                // We store all the .gouv.fr domain in an array cause fast-csv parse in a async way
                // and we need to parse .gouv.fr domain one by one in a sync way with puppeteer
                gouvUrls.push(url);
            }
        })
        .on('end', async (rowCount: number) => {     // When all the csv is parsed
            for (const url of gouvUrls) {              // For each .gouv.fr domain
                try {
                    await setTimeout(() => {}, 1000);  // We wait 1 second to avoid the server to block the request
                    const response = await page.goto(`https://${url}`, {waitUntil: "networkidle0"})

                    if (response.status() === 200) {  // Second step : check if the domain is active (is the server respond with a 200 code).
                        activeGouvUrls.push(url);

                        // Third step : check if the domain redirect to another domain
                        // Because each domain can redirect to another we have to keep only the unique domain
                        // for exemple, actually (2022/06/04), the domain ecologique-solidaire.gouv.fr and ecologie-solidaire.gouv.fr is redirect to ecologie.gouv.fr
                        // so we have to keep only the unique domain (ecologie.gouv.fr)
                        // The Set handle that for us. If the domain is already in the set, it will not add another one
                        const host = new URL(response.url()).host
                        console.log(`${url} is active with host ${host}`)
                        uniqueAndActiveGouvUrls.add(host);
                    } else if (response) { // If the server respond with a code different than 200, we add it for a manual check
                        needACheckGouvUrls.push(url);
                        console.log(`${url} need a manual check`)
                    }
                } catch (e) {
                    console.log(`${url} is not active`);
                }
            }

            console.log(`Recap / ${rowCount} urls / ${gouvUrls.length} gouv.fr urls / ${activeGouvUrls.length} active gouv.fr urls / ${uniqueAndActiveGouvUrls.size} unique and active gouv.fr urls / ${needACheckGouvUrls.length} need a manual check`);

            // ----------- Add the header for the csv (The header is the first line of the tab) --------
            const uniqueAndActiveGouvUrlsArray = Array.from(uniqueAndActiveGouvUrls.keys());
            uniqueAndActiveGouvUrlsArray.unshift('url');
            needACheckGouvUrls.unshift('url');
            // ------------ Write the output file ------------
            const outputFilePath = getOutputFileDataPath('active-unique-gouv-fr-domain');
            await writeToPath(outputFilePath, uniqueAndActiveGouvUrlsArray.map(url => [url]));

            const outputFileManualPath = getOutputFileDataPath('need-check-with-another-ip-gouv-fr-domain');
            await writeToPath(outputFileManualPath, needACheckGouvUrls.map(url => [url], {headers: true, delimiter: ";"}));

            console.log('Done ! Output file : ' + outputFilePath + ' ' + 'Manual check with another ip : ' + needACheckGouvUrls.length);
            await browser.close()
        })
})();
