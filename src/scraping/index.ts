import * as csvParser from '@fast-csv/parse';
import {DEFAULT_SCRAPED_WEBSITE, ScrapedWebsite} from "../models/ScrapedWebsite";
import startScraping from "./Scraping";
import {getInputFileDataPath} from "../FileManager";

// Main function to parse the csv file and launch the scraping
(async () => {
    const INPUT_FILE_NAME = process.argv[2];         // the input filename
    const COLUMN_NAME_URL = process.argv[3];         // the column name of the url in the input file

    // Log error if missing argument
    if (!INPUT_FILE_NAME || !COLUMN_NAME_URL) {
        console.error("Usage: npm run start:scrap <input-file-name> <column-name-url>. See the README.md for more information.");
        process.exit(1);
    }

    const scrapedWebsites: ScrapedWebsite[] = [];    // Fast-csv parse in a async way and we need to be sync for puppeteer, so we store all the website in an array

    console.log('Parsing csv...');
    csvParser.parseFile(getInputFileDataPath(INPUT_FILE_NAME), {delimiter: ';', headers: true})
        .on('error', (error) => {console.error(error)})
        .on('data', async (row: any) => {           // For each row in the csv
            const url = row[COLUMN_NAME_URL]                     // Get the url from the csv
            const scrapedWebsite = DEFAULT_SCRAPED_WEBSITE(url); // Create a default scraped website with the url
            scrapedWebsites.push(scrapedWebsite);
        })
        .on('end', () => {                          // When all the csv is parsed start the scraping
            console.log('Parsing csv done! Starting to scrape...');
            startScraping(scrapedWebsites)
        })
})();
