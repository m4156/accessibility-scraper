import {
    ACCESSIBILITY_CONFORMITY,
    getAccessibilityConformityFromPercent,
    ScrapedWebsite
} from "../models/ScrapedWebsite";
import {Page} from "puppeteer";

// This function scrapes a website
// The scrap is divides in two parts :
// - Homepage : more details in the code
// - Accessibility page : more details in the code
const scrapWebsite = async (scrapedWebsite: ScrapedWebsite, page: Page) => {
    // ---------------------------------------------------------------------------------------------------------------------
    // First part : Homepage.
    // This part aims to get the following information :
    // - If the homepage has a link to the accessibility page
    // - The url of the accessibility page.
    await page.goto(`https://${scrapedWebsite.url}`, {waitUntil: 'networkidle0', timeout: 30000});
    console.log(`Page loaded for ${scrapedWebsite.url}`);

    const result = await page.$$eval('a',(elements) => {   // this function is executed in the browser context, that why we return the result as object and add it to the scrapedWebsite
        const accessibilityRegex1 = /(totalement|partiellement|non-?)\s?conforme/gmi  // Best regex
        const accessibilityRegex2 = /(accessibility|accessibilité)/gmi                // Second best regex

        // @ts-ignore no ts error because we are in the browser context
        let lastResult, bestResult;
        elements.forEach(element => {
            // @ts-ignore href is known in the page context
            const href = element?.href

            // @ts-ignore no ts error because we are in the browser context
            if (element.textContent && href && !bestResult) {
                if (accessibilityRegex1.test(element.textContent)) { // If the text content match the first result, return directly
                    const homepage_accessibility = true
                    const accessibility_page_url = href

                    bestResult = {accessibility_page_url, homepage_accessibility}
                } else if (accessibilityRegex2.test(element.textContent)) {
                    const homepage_accessibility = true
                    const accessibility_page_url = href

                    lastResult = {accessibility_page_url, homepage_accessibility}
                    console.log(lastResult, new Date())
                }
            }
        });


        // Return the best result if is not undefined
        return bestResult ? bestResult : lastResult;
    });

    if (result === undefined) {
        throw new Error(`No Accessibility link found`);
    } else {
        Object.assign(scrapedWebsite, result);
    }

    // ---------------------------------------------------------------------------------------------------------------------
    // Second part : Accessibility page.
    // This part aims to get the following information :
    // - If there is a RGAA audit
    // - The accessibility percent.
    // - The accessibility conformity from the accessibility percent.
    // - The accessibility average percent per page.
    if (scrapedWebsite.accessibility_page_url) {
        await page.goto(scrapedWebsite.accessibility_page_url, {waitUntil: 'load', timeout: 30000});
        console.log(`Accessibility page loaded for ${scrapedWebsite.accessibility_page_url}`);

        const pageContent = await page.content()

        scrapedWebsite.accessibility_percent = findAccessibilityPercent(pageContent)
        scrapedWebsite.accessibility_conformity = getAccessibilityConformityFromPercent(scrapedWebsite.accessibility_percent)
        scrapedWebsite.audited = true;   // If there is a accessibility_percent found, the website is audited.

        // The accessibility average percent is optional for websites
        scrapedWebsite.accessibility_average_rate_percent = findAccessibilityAverageRatePercent(pageContent)
    } else {
        throw new Error(`No Accessibility page found`);
    }
}

// This method is used to find the accessibility percent (Pourcentage des critères RGAA respectés)
// Content is a html page
const findAccessibilityPercent = (content: string): number => {
    // these regexps are used to find the accessibility percent (Pourcentage des critères RGAA respectés)
    // We need two of them because, sometimes, the accessibility percent is not displayed in the same way
    const percentRegex1 = /(?<percent>\b(?<!(\.|\,))(?!0+(?:(\.|\,)0+)?%)(?:\d|[1-9]\d|100)(?:(?<!100)(\.|\,)\d+)?)\s?%\sdes\scritères\s(?:du\s)?RGAA\s(?:.*\s)?sont\s(?:respectés|conformes)/ig;
    const percentRegex2 = /révèle\sque\sle\ssite\sest\sconforme\sà\s(?<percent>\b(?<!(\.|\,))(?!0+(?:(\.|\,)0+)?%)(?:\d|[1-9]\d|100)(?:(?<!100)(\.|\,)\d+)?)\s?%\sau\sRGAA/gi;

    const results = Array.from(content.matchAll(percentRegex1)).map(match => match?.groups?.percent)
            .concat(Array.from(content.matchAll(percentRegex2)).map(match => match?.groups?.percent));

    return sanitizeAndGetAverageFromResults(results, 'accessibility percent');
}

// This method is used to find the accessibility average rate percent (Taux moyen de conformité du service en ligne)
// Content is a html page
const findAccessibilityAverageRatePercent = (content: string): number => {
    // regexp used to find the accessibility average rate percent (Taux moyen de conformité du service en ligne)
    const percentAverageRateRegex = /le\staux\smoyen\sde\sconformité\s(?:du\sservice\sen\sligne\ss’élève\sà\s)?(?:du\ssite\sest\sde\s)?(?<percent>\b(?<!(\.|\,))(?!0+(?:(\.|\,)0+)?%)(?:\d|[1-9]\d|100)(?:(?<!100)(\.|\,)\d+)?)%/gi;
    const results = Array.from(content.matchAll(percentAverageRateRegex)).map(match => match?.groups?.percent);

    return sanitizeAndGetAverageFromResults(results, 'accessibility percent rate average');
}

// Sanitize the results and return the average as number
const sanitizeAndGetAverageFromResults = (results: (string | undefined)[], name: string): number => {
    // If there are multiple result we take the average
    // Because sometimes website have multiples audits, so we take the average of all the results
    if (results.length >= 1) {
        const sanitizedResults = results.map(result => {
            if (result) {
                return sanitizePercentString(result, name)
            } else {
                throw new Error(`Result is undefined : ${result}`);
            }
        })

        // Average calculation
        const sum = sanitizedResults.reduce((a, b) => a + b, 0);
        return sum / sanitizedResults.length;
    } else {
        throw new Error(`No percent found for the ${name}`);
    }
}

// Sanitize (modify) the percent string into a number
const sanitizePercentString = (percentString: string, name: string): number => {
    const sanitizedPercentString = percentString
        .trim()
        .replace(',', '.')
        .replace(' ', '')

    const percent = Number(sanitizedPercentString)

    // Check if the percent is invalid (not undefined, not NaN, between 0 and 100)
    if (!percent || isNaN(percent) || percent < 0 || percent > 100) {
        throw new Error(`Invalid percent: ${percentString}/${sanitizedPercentString}/${percent} for regex ${name}`);
    }

    return percent;
}

export default scrapWebsite
