import {ScrapedWebsite} from "../models/ScrapedWebsite";
import puppeteer from "puppeteer";
import scrapWebsite from "./ScrapWebsite";
import {getExtensionIDontCareAboutCookiesPath, getOutputFileDataPath} from "../FileManager";
import {writeToPath} from "@fast-csv/format";

const launchScrap = async (scrapedWebsites: ScrapedWebsite[]) => {
    // ---------------------------------- Start browser ----------------------------------
    console.log('Starting browser...');
    const browser = await puppeteer.launch({
        headless: false,
        defaultViewport: {
            width: 1920,
            height: 1080
        },
        args: [
            `--disable-extensions-except=${getExtensionIDontCareAboutCookiesPath()}`,
            `--load-extension=${getExtensionIDontCareAboutCookiesPath()}`,
            '--lang=fr-FR,fr'
        ]
    });
    const page = await browser.newPage();

    // -----------------------------------  For every website do a scraping -----------------------------------
    for (let scrapedWebsite of scrapedWebsites) {
        try {
            await scrapWebsite(scrapedWebsite, page);
        } catch (e) {    // If a error occurs during the scrap, catch the error and write the error in the csv
            scrapedWebsite.err_desc = (e as Error).message;
        }
        console.log(scrapedWebsite);
    }

    console.log('End of scraping ! Closing browser...');
    await browser.close();

    // --------------------------------------- Write the result in a CSV file -----------------------------------
    const outputFilePath = getOutputFileDataPath('scraping');
    await writeToPath(outputFilePath, scrapedWebsites, {headers: true, delimiter: ";"});
    console.log('Done ! Output file : ' + outputFilePath);
}

export default launchScrap;
