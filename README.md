# Les Outils utilisés
- **NodeJS** / Pour l'exécution du code (`npm install` une fois le code récupéré)
- **NPM** / Pour la gestion des dépendances
- **FastCSV** / Pour la lecture et l'écriture de ficher CSV
- **Puppeteer** / Le navigateur utilisé pour aller sur les pages web

# Les fichiers d'entrée et de sortie
L'ensemble des fichiers d'entrée et de sortie se trouve dans le dossier [data](data).

# La liste des sites gouvernementaux
## Fichier d'entrée
Dans un premier temps on récupère une liste de domaines web sour le format **CSV**. Ici la liste de tout les domaines web en ".fr".

- https://www.afnic.fr/produits-services/services-associes/donnees-partagees/

On le place dans le dossier **[data](data)**. Il garde son nom d'origine [202203_OPENDATA_A-NomsDeDomaineEnPointFr.csv](data/202203_OPENDATA_A-NomsDeDomaineEnPointFr.csv)
## Lancement de la récupération des sites gouvernementaux
Le script se trouve dans le fichier [src/gouv-fr-domain/index](src/gouv-fr-domain/index.ts) et s'exécute avec la commande `npm run start:gouv` avec les paramètres suivants :
1. Le nom du fichier d'entrée
2. Le nom de la colonne contenant les domaines

Pour résumer :
`npm run start:gouv <nom-du-fichier-d-entree> <col-name-url>`

Exemple avec le fichier précédent :
`npm run start:gouv 202203_OPENDATA_A-NomsDeDomaineEnPointFr.csv "Nom de domaine"`

## Explication sur le script
Le script va effectuer plusieurs tris :
 1. Premier tri pour récupérer que les domaines se terminant par .gouv.fr
 2. Deuxième tri pour retirer les domaines qui ne mène à rien
 3. Troisième tri pour retirer les domaines qui redirige vers une autre URL pour ne retenir que les domaines uniques

L'étape 2 et 3, selon le nombre de domaine à tester, celà peut prendre un certain temps.
Pour l'exemple ci dessus la script a mis 1h10 pour s'éxécuter avec comme résultat :
7355499 urls / 1101 gouv.fr urls / 233 active gouv.fr urls / 198 unique and active gouv.fr urls et 69 à vérifier manuellement.


## Fichier de sortie
Il y a deux fichiers de sortie :
- `data/output_active-unique-gouv-fr-domain${timestamp}.csv` : La liste des domaines actifs et uniques
- `data/output_need-manual-check-gouv-fr-domain${timestamp}.csv` : La liste des ou un serveur a répondu avec autre chose que 200

Le principe est de vérifier un par un les domaines de la dernière liste via une autre IP, car dû aux protections DDOS (surtout sur les sites de région) il est possible que l'ip utilisée par le script soit provisoirement ban, puis de les ajouter à l'autre liste s'ils sont actifs. Attention à vérifier que le domaine ne redirige pas vers une autre URL déjà dans la liste.

J'ai effectué les vérifications pour et j'ai concaténé les résultats pour l'exemple dans le fichier : 
`output_active-gouv-fr-domain_checked_20220605T130642576Z.csv`

# Scraping
## Fichier d'entrée
L'outil de scraping prend un csv contenant des urls à analyser en fichier d'entrée. Le fichier doit être dans le dossier **[data](data)**.

## Lancement du scraping
Les fichiers de scrapping se trouvent dans le dossier [src/scrapping](src/scraping) et s'exécute avec la commande `npm run start:scrap` avec les paramètres suivants :
1. Le nom du fichier d'entrée
2. Le nom de la colonne contenant les domaines

Pour résumer :
`npm run start:scrap <nom-du-fichier-d-entree> <col-name-url>`

Exemple avec le fichier précédent :
`npm run start:scrap output_active-gouv-fr-domain_checked_20220605T130642576Z.csv url`

## Explication sur le script
Le script va :
1. Se rendre sur l'URL
2. Chercher à l'aide d'une REGEX une page d'accessibilité
3. Se rendre sur la page d'accessibilité
4. Récupérer les informations recherchées à l'aide de REGEX

Les informations recherchées sont :
- Si l'url à un lien vers une page d'accessibilité (boolean)
- L'url de la page d'accessibilité (string)
- Si le site dispose d'un audit RGAA (boolean)
- L'état de conformité calculé (ENUM : TOTALEMENT_CONFORME, PARTIELLEMENT_CONFORME, NON_CONFORME)
- Le pourcentage des critères respectés (decimal)
- Le taux moyen du service en ligne, si présent (decimal)
- L'erreur s'il y en a une

## Fichier de sortie
Après que le scrip soit exécuté, tous les résultats sont compilés dans un seul fichier avec le nom `data/output_scraping_${timestamp}.csv`

# Les points à améliorer
## La détection des informations
- La détection des critères respectés et taux moyen à l'aide des REGEX (Le fichier [data/improve-detection/exemple_percent.txt](data/improve-detection/exemple_percent.txt) pour connaitre les formulations)
- Rendre cette détection multilingue (au moins en anglais)


# Mentions spéciales

Au console.log('okok') oublié sur la page des impots. (3 mai 2022 à 20h)
![img.png](https://media.discordapp.net/attachments/763349360523214848/971108356355948575/unknown.png)


## La liste des villes

La base de la liste
- https://www.data.gouv.fr/fr/datasets/service-public-fr-annuaire-de-l-administration-base-de-donnees-locales/

Statistiques sur les données :
- https://www.polipart.fr/statistiques

